package com.sportplatzmedia.sportlerplus.app;

import android.content.res.Configuration;
import android.util.Log;

import com.google.firebase.FirebaseApp;


import androidx.multidex.MultiDexApplication;

public class SportlerPlusApplication extends MultiDexApplication {
    private static final String TAG = "SportlerPlus";

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Start");
        FirebaseApp.initializeApp(this);
    }
}
