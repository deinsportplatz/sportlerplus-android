package com.sportplatzmedia.sportlerplus.googlefit;

import com.google.android.gms.fitness.FitnessActivities;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class GFWorkoutSessionSerializer implements JsonSerializer<GFWorkoutSession> {
    final static int GOOGLE_FIT_IMPORT_SOURCE = 2;

    @Override
    public JsonElement serialize(GFWorkoutSession gfs, Type type, JsonSerializationContext context) {
        JsonObject root = new JsonObject();
        root.addProperty("workout_id", gfs.workoutId);
        root.addProperty("import_source", GOOGLE_FIT_IMPORT_SOURCE);
        root.addProperty("source_app", gfs.sourceApp);
        root.addProperty("description", gfs.description);
        root.addProperty("from_utc", gfs.fromUTC);
        root.addProperty("to_utc", gfs.toUTC);
        root.addProperty("name", gfs.name);
        root.addProperty("type", translateActivity(gfs.type));
        root.addProperty("duration", gfs.activityMinutes * 60);
        JsonObject energy = new JsonObject();
        energy.addProperty("value", gfs.energyAsKcal);
        energy.addProperty("as", "kCal");
        root.add("energy", energy);
        JsonObject distance = new JsonObject();
        distance.addProperty("value", gfs.distanceAsM);
        distance.addProperty("as", "m");
        root.add("distance", distance);
        return root;
    }

    public static String translateActivity(String a) {
        if (a == null) { return ""; }
        switch(a) {
            case FitnessActivities.AEROBICS:
                return "Aerobic";
            case FitnessActivities.WALKING:
                return "Gehen";
            case FitnessActivities.BIKING:
                return "Radfahren";
            case FitnessActivities.FOOTBALL_SOCCER:
                return "Fußball";
            case FitnessActivities.SURFING:
                return "Surfen";
            case FitnessActivities.WEIGHTLIFTING:
                return "Gewichtheben";
            case FitnessActivities.RUNNING:
                return "Laufen";
            case FitnessActivities.RUNNING_JOGGING:
                return "Joggen";
            case FitnessActivities.OTHER:
                return "Sonstiges";
            case FitnessActivities.TABLE_TENNIS:
                return "Tischtennis";
            default:
                return a.substring(0, 1).toUpperCase() + a.substring(1);
        }
    }

}
