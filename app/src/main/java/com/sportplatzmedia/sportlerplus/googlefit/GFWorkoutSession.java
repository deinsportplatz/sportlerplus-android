package com.sportplatzmedia.sportlerplus.googlefit;

public class GFWorkoutSession {
    public String workoutId;
    public String fromUTC;
    public String toUTC;
    public String name;
    public String type;
    public String description;
    public String sourceApp;
    public Integer activityMinutes = 0;
    public float distanceAsM = 0f;
    public float energyAsKcal = 0f;
}
