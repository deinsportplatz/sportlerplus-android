package com.sportplatzmedia.sportlerplus.googlefit;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.SessionReadRequest;
import com.google.android.gms.fitness.result.SessionReadResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleFitHelper {

    private static final String TAG = "GoogleFitHelper";
    private static final GoogleFitHelper gfh = new GoogleFitHelper();
    private static final String GOOGLE_FIT_PACKAGE_NAME = "com.google.android.apps.fitness";
    public static final int REQUEST_OAUTH_CODE = 101;
    public static final int REQUEST_PERMISSIONS_CODE = 102;
    private GoogleSignInAccount account;
    private ArrayList<String> activityBlackList;
    private Context mContext;
    private boolean hadError = false;


    private GoogleFitHelper() {
        Log.d(TAG, "Instance created");
    }

    public void init(Context mContext) {
        this.mContext = mContext;
        activityBlackList = new ArrayList<>();
        activityBlackList.add(FitnessActivities.SLEEP);
        activityBlackList.add(FitnessActivities.SLEEP_AWAKE);
        activityBlackList.add(FitnessActivities.SLEEP_DEEP);
        activityBlackList.add(FitnessActivities.SLEEP_LIGHT);
        activityBlackList.add(FitnessActivities.SLEEP_REM);
    }
    public static GoogleFitHelper getInstance() {
        return gfh;

    }
    /**
     * Checks if google fit is installed on device, normally all devices with android 2.3 or higher & gms have installed the fit app
     * @return Boolean, true if the package is installed
     */
    public Boolean getIsAvailable() {
        try {
            mContext.getPackageManager().getPackageInfo(GOOGLE_FIT_PACKAGE_NAME, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private boolean hasOAuthPermission() {
        FitnessOptions fitnessOptions = getFitnessOptions();
        account = GoogleSignIn.getAccountForExtension(this.mContext, fitnessOptions);
        if (!GoogleSignIn.hasPermissions(account, fitnessOptions)) {
            return false;
        }
        return true;
    }

    public void requestOAuthPermission() {
        Log.d(TAG, "checking OAuth permissions");
        FitnessOptions fitnessOptions = getFitnessOptions();
        account = GoogleSignIn.getAccountForExtension(mContext, fitnessOptions);

        if (!GoogleSignIn.hasPermissions(account, fitnessOptions) || hadError) {
            //Need Google permissions
            Log.d(TAG, "no permissions yet, requesting...");
            requestOAuth(fitnessOptions);
        } else {
            Log.d(TAG, "permissions granted");
            //Has permissions
            //callback.onOAuthGranted();
        }
    }

    private void requestOAuth(FitnessOptions fitnessOptions) {
        //Result of requestPermissions will be handled by onActivityResult
        GoogleSignIn.requestPermissions((Activity) mContext, REQUEST_OAUTH_CODE, account, fitnessOptions);
    }

    private FitnessOptions getFitnessOptions() {
        return FitnessOptions.builder()
                .accessActivitySessions(FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_MOVE_MINUTES, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_SPEED, FitnessOptions.ACCESS_READ)
                .build();
    }

    public Date atEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public Date atStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public Date startOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek() -  calendar.get(Calendar.DAY_OF_WEEK));
        return calendar.getTime();
    }

    public Date endOfWeek(Date date) {
        Calendar ce = Calendar.getInstance();
        ce.setTime(startOfWeek((date)));
        ce.add(Calendar.DAY_OF_YEAR, 6);
        ce.set(Calendar.HOUR_OF_DAY, 23);
        ce.set(Calendar.MINUTE, 59);
        ce.set(Calendar.SECOND, 59);
        ce.set(Calendar.MILLISECOND, 999);
        return ce.getTime();
    }

    private List<GFWorkoutSession> processSession(SessionReadResponse rawSessionResponse) {
        List<GFWorkoutSession> result = new ArrayList<GFWorkoutSession>();
        List<Session> sessions = rawSessionResponse.getSessions();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for (Session session : sessions) {
            if (activityBlackList.contains(session.getActivity())) {
                Log.i(TAG, "Session activity " + session.getActivity() + " is in blacklist");
            } else {
                Date start = new Date();
                start.setTime(session.getStartTime(TimeUnit.MILLISECONDS));
                Date end = new Date();
                end.setTime(session.getEndTime(TimeUnit.MILLISECONDS));

                Integer activityMinutes = 0;
                float distanceAsM = 0f;
                float energyAsKcal = 0f;

                List<DataSet> dataSets = rawSessionResponse.getDataSet(session);
                for (DataSet dataSet: dataSets) {
                    //Log.i(TAG, "Data returned for Data type: " + dataSet.getDataType().getName() + " from: " + dataSet.getDataSource().getAppPackageName());
                    for (DataPoint dp : dataSet.getDataPoints()) {
                        switch (dataSet.getDataType().getName()) {
                            case "com.google.active_minutes":
                                if (dp.getValue(Field.FIELD_DURATION).asInt() == 1) {
                                    activityMinutes++;
                                }
                                break;
                            case "com.google.distance.delta":
                                distanceAsM += dp.getValue(Field.FIELD_DISTANCE).asFloat();
                                break;
                            case "com.google.calories.expended":
                                energyAsKcal += dp.getValue(Field.FIELD_CALORIES).asFloat();
                                break;
                            default:
                                //throw new IllegalStateException("Unexpected value: " + dataSet.getDataType().getName());
                                Log.d(TAG, "Unexpected value: " + dataSet.getDataType().getName());
                        }
                        /*Log.i(TAG, "Data point:");
                        Log.i(TAG, "\tType: " + dp.getDataType().getName());
                        Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                        Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                        for (Field field : dp.getDataType().getFields()) {
                            Log.i(TAG, "\tField: " + field.getName() + " Value: " + dp.getValue(field));
                        }*/
                    }
                }

                Log.i(TAG, "------------------------------------------------------");
                Log.i(TAG, "\tSession Start: " + start);
                Log.i(TAG, "\tSession End: " + end);
                Log.i(TAG, "\tSession Activity: " + session.getActivity());
                Log.i(TAG, "\tSession Name: " + session.getName());
                Log.i(TAG, "\tSession Description: " + session.getDescription());
                Log.i(TAG, "\tApp Name: " + session.getAppPackageName());
                Log.i(TAG, "\ttotal energy: " + energyAsKcal);
                Log.i(TAG, "\ttotal distance: " + distanceAsM);
                Log.i(TAG, "\ttotal minutes: " + activityMinutes);
                Log.i(TAG, "------------------------------------------------------");

                GFWorkoutSession gfs = new GFWorkoutSession();
                gfs.activityMinutes = activityMinutes;
                gfs.distanceAsM = distanceAsM;
                gfs.energyAsKcal = energyAsKcal;
                gfs.fromUTC = dateFormat.format(session.getStartTime(TimeUnit.MILLISECONDS));
                gfs.toUTC = dateFormat.format(session.getEndTime(TimeUnit.MILLISECONDS));
                gfs.type = session.getActivity();
                gfs.description = session.getDescription();
                gfs.sourceApp = session.getAppPackageName();
                gfs.name = session.getName();
                gfs.workoutId = "" + session.getStartTime(TimeUnit.MILLISECONDS);
                result.add(gfs);
            }
        }

        return result;
    }

    // Function to retrieve workout data for a specific day with expended calories, workout time, and distance
    public void getWorkoutsForDateRange(Date sd, Date ed, GoogleFitCallbacks callback) {

        long startTime = atStartOfDay(sd).getTime();
        long endTime = atEndOfDay(ed).getTime();

        SessionReadRequest readRequest = new SessionReadRequest.Builder()
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
                .read(DataType.TYPE_DISTANCE_DELTA)
                .read(DataType.TYPE_MOVE_MINUTES)
                .read(DataType.TYPE_CALORIES_EXPENDED)
                .readSessionsFromAllApps()
                .enableServerQueries()
                .build();

        Fitness.getSessionsClient(this.mContext, account)
                .readSession(readRequest)
                .addOnSuccessListener(new OnSuccessListener<SessionReadResponse>() {
                    @Override
                    public void onSuccess(SessionReadResponse sessionReadResponse) {
                        // Get a list of the sessions that match the criteria to check the result.
                        List<GFWorkoutSession> gfsl;
                        gfsl = processSession(sessionReadResponse);
                        callback.onWorkoutsReceived(gfsl);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, "Failed to read session");
                        hadError = true;
                        callback.onWorkoutsError();
                    }
                });
    }

    //Disconnect Fit from app
    public void disconnect() {
        Fitness.getConfigClient(mContext,  GoogleSignIn.getAccountForExtension(mContext, getFitnessOptions()))
                .disableFit().addOnSuccessListener(
                        unused ->
                                Log.i(TAG, "Disabled Google Fit"))

                .addOnFailureListener(e ->
                        Log.w(TAG, "There was an error disabling Google Fit", e));

    }
}