package com.sportplatzmedia.sportlerplus.googlefit;

import java.util.List;

public interface GoogleFitCallbacks {
    void onWorkoutsReceived(List<GFWorkoutSession> sessionList);
    void onWorkoutsError();
}
