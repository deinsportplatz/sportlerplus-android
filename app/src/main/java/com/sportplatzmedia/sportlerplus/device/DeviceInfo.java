package com.sportplatzmedia.sportlerplus.device;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;

import java.math.BigInteger;
import java.security.MessageDigest;

public class DeviceInfo {
    public static String getDeviceInfo(Context activity, Device device) {
        try {
            switch (device) {
                case DEVICE_ID:
                    return getDeviceId(activity);
                case APP_VERSION:
                    return getAppVersion(activity);
                case APP_BUILD:
                    return getAppBuild(activity);
                case DEVICE_HARDWARE_MODEL:
                    return getDeviceName();
                case DEVICE_NUMBER_OF_PROCESSORS:
                    return Runtime.getRuntime().availableProcessors() + "";
                case DEVICE_MANUFACTURE:
                    return Build.MANUFACTURER;
                case DEVICE_VERSION:
                    return String.valueOf(Build.VERSION.SDK_INT);
                case DEVICE_SYSTEM_VERSION:
                    return String.valueOf(getDeviceName());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "";
    }

    private static String getDeviceId(Context ctx) {
        @SuppressLint("HardwareIds") String device_uuid = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (device_uuid == null) {
            device_uuid = "123456789"; // for testing (emulator)
        } else {
            try {
                byte[] _data = device_uuid.getBytes();
                MessageDigest _digest = java.security.MessageDigest.getInstance("MD5");
                _digest.update(_data);
                _data = _digest.digest();
                BigInteger _bi = new BigInteger(_data).abs();
                device_uuid = _bi.toString();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return device_uuid;
    }

    private static String getAppVersion(Context ctx) {
        String appVersion = "";
        try {
            appVersion = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return appVersion;
    }

    private static String getAppBuild(Context ctx) {
        String appBuild = "";
        try {
            appBuild = Integer.toString(ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return appBuild;
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }

        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    private static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;

        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(model);
        }
    }
}
