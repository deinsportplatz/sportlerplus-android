package com.sportplatzmedia.sportlerplus.device;

public enum Device {
    DEVICE_ID,
    DEVICE_VERSION,
    DEVICE_SYSTEM_VERSION,
    DEVICE_MANUFACTURE,
    DEVICE_HARDWARE_MODEL,
    DEVICE_NUMBER_OF_PROCESSORS,
    APP_VERSION,
    APP_BUILD
}
