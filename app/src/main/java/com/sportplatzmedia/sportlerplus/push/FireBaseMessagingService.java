package com.sportplatzmedia.sportlerplus.push;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class FireBaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FireBaseMessagingService";

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferencesConfig sharedPreferencesConfig = new SharedPreferencesConfig();
        try {
            Log.i(TAG, "FCM Registration Token: " + token);
            sharedPreferences.edit().putString(sharedPreferencesConfig.PUSH_TOKEN, token);
            sharedPreferences.edit().putBoolean(sharedPreferencesConfig.PUSH_TOKEN_IS_FCM, true).apply();
            sharedPreferences.edit().putBoolean(sharedPreferencesConfig.SEND_TOKEN_TO_SERVER, true).apply();
        } catch (Exception ex) {
            Log.i(TAG, "Failed to complete token refresh " , ex);
            sharedPreferences.edit().putString(sharedPreferencesConfig.PUSH_TOKEN,"");
            sharedPreferences.edit().putBoolean(sharedPreferencesConfig.SEND_TOKEN_TO_SERVER, false).apply();
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {    // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            String uri = data.get("uri");
            Log.d(TAG, "Target URI:" + uri);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
}
