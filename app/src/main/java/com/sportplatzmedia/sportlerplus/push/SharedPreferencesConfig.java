package com.sportplatzmedia.sportlerplus.push;

public class SharedPreferencesConfig {
    public static final String SEND_TOKEN_TO_SERVER = "sendTokenToServer";
    public static final String PUSH_TOKEN = "gcmToken";
    public static final String PUSH_TOKEN_IS_FCM = "isFcmToken";

}
