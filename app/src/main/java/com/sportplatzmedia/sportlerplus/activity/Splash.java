package com.sportplatzmedia.sportlerplus.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.net.URI;
import java.net.URISyntaxException;

import spm.sportlerplus.R;


public class Splash extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;            //set your time here......
    private static final String TAG = Splash.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final String startUrl;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //bundle must contain all info sent in "data" field of the notification
            startUrl = extras.getString("uri");
            Log.d(TAG, "startup url received: " + startUrl);
        }
        else {
            startUrl = null;
        }

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(Splash.this,MainActivity.class);
                if (startUrl != null) {
                    mainIntent.setAction("android.intent.action.VIEW");
                    mainIntent.setData(Uri.parse(startUrl));
                }
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}