package com.sportplatzmedia.sportlerplus.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.Manifest;
import spm.sportlerplus.R;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sportplatzmedia.sportlerplus.googlefit.GFWorkoutSession;
import com.sportplatzmedia.sportlerplus.googlefit.GFWorkoutSessionSerializer;
import com.sportplatzmedia.sportlerplus.googlefit.GoogleFitCallbacks;
import com.sportplatzmedia.sportlerplus.googlefit.GoogleFitHelper;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MainActivity extends FragmentActivity {
    public static final String baseUrl = "https://sportler-plus.web.app/";
    //public static final String baseUrl = "https://test.sportlerplus.com/";
    // Static UA
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.101 Mobile Safari/537.36";

    // Elements
    private WebView webView;
    public long checkedInappPurchases;
    private static final int INPUT_FILE_REQUEST_CODE = 1;
    private static final int FILECHOOSER_RESULTCODE = 1;

    private static final String TAG = MainActivity.class.getSimpleName();
    private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    public static void verifyGoogleFitPermissions(Activity activity) {
        // Check if we have write permission
        int permission1 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        int permission2 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACTIVITY_RECOGNITION);

        if (permission1 != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    GoogleFitHelper.REQUEST_PERMISSIONS_CODE);
        }
        if (permission2 != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACTIVITY_RECOGNITION},
                    GoogleFitHelper.REQUEST_PERMISSIONS_CODE);
        }
    }

    public void evaluateJS(String scriptToExecuse) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript(scriptToExecuse, null);
        }
        else {
            webView.loadUrl("javascript:" + scriptToExecuse);
        }
    }

    private void transferDeviceToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        String installationId = token.split(":")[0];
                        Log.d(TAG, "FCM token: " + token);
                        // Log and transfer device token
                        String tokenJS = String.format("if (window.vm && window.vm.homeComponent) { window.vm.homeComponent.setDeviceToken(\"%s\", \"Android\", \"%s\") }",
                            token, installationId);
                        MainActivity.this.evaluateJS(tokenJS);
                    }
                });
    }

    // [START ask_post_notifications]
    // Declare the launcher at the top of your Activity/Fragment:
    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    // FCM SDK (and your app) can post notifications.
                } else {
                    // TODO: Inform user that that your app will not show notifications.
                }
            });

    private void askNotificationPermission() {
        // This is only necessary for API level >= 33 (TIRAMISU)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) ==
                    PackageManager.PERMISSION_GRANTED) {
                // FCM SDK (and your app) can post notifications.
            } else if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {
                // TODO: display an educational UI explaining to the user the features that will be enabled
                //       by them granting the POST_NOTIFICATION permission. This UI should provide the user
                //       "OK" and "No thanks" buttons. If the user selects "OK," directly request the permission.
                //       If the user selects "No thanks," allow the user to continue without notifications.
            } else {
                // Directly ask for the permission
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS);
            }
        }
    }
    // [END ask_post_notifications]

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GoogleFitHelper.REQUEST_PERMISSIONS_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "permissions granted");
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == INPUT_FILE_REQUEST_CODE && mFilePathCallback != null) {
                Uri[] results = null;
                // Check that the response is a good one
                if (resultCode == Activity.RESULT_OK) {
                    if (data == null) {
                        // If there is not data, then we may have taken a photo
                        if (mCameraPhotoPath != null) {
                            results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                        }
                    } else {
                        String dataString = data.getDataString();
                        if (dataString != null) {
                            results = new Uri[]{Uri.parse(dataString)};
                        }
                    }
                }
                mFilePathCallback.onReceiveValue(results);
                mFilePathCallback = null;
            }
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (requestCode == FILECHOOSER_RESULTCODE && mUploadMessage != null) {
                Uri result = null;
                try {
                    if (resultCode != RESULT_OK) {
                        result = null;
                    } else {
                        // retrieve from the private variable if the intent is null
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "activity :" + e,
                            Toast.LENGTH_LONG).show();
                }
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
    }

    private File createImageFile() throws IOException {
        verifyStoragePermissions(MainActivity.this);

        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return imageFile;
    }

    private String createCacheDir() {
        File dir = getCacheDir();

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir.getPath();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = getApplicationContext();
        //final GoogleBillingClient googleBillingClient = new GoogleBillingClient(context, MainActivity.this);

        GoogleFitHelper.getInstance().init(this);

        CookieManager.getInstance().setAcceptCookie(true);
        webView = (WebView) findViewById(R.id.web_view);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);

        // WebSettings
        final WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        //webSettings.setAppCacheEnabled(true);
        //webSettings.setAppCachePath(createCacheDir());
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setAllowFileAccess(true);
        webSettings.setMediaPlaybackRequiresUserGesture(false);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                transferDeviceToken();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("shouldOverrideUrlLoading: " + url);
                /*if (url.startsWith("inapppurchase://")) {
                    if (url.contains("subscription3m")) {
                        googleBillingClient.purchaseProduct("com.sportplatzmedia.sportlerplus.subscription3m", BillingClient.SkuType.SUBS);
                    } else if (url.contains("subscription12m")) {
                        googleBillingClient.purchaseProduct("com.sportplatzmedia.sportlerplus.subscription12m", BillingClient.SkuType.SUBS);
                    }
                    return true;
                }
                else */
                if (url.startsWith("webcal://")) {
                    MainActivity.this.openCalendarApp(url);
                    return true;
                }
                else {
                    view.loadUrl(url);
                    return false;
                }
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, WebChromeClient.FileChooserParams fileChooserParams) {
                // Double check that we don't have any existing callbacks
                if (mFilePathCallback != null) {
                    mFilePathCallback.onReceiveValue(null);
                }
                mFilePathCallback = filePath;
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        Log.e(TAG, "Unable to create Image File", ex);
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile));
                    } else {
                        takePictureIntent = null;
                    }
                }
                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentSelectionIntent.setType("image/*");
                Intent[] intentArray;
                if (takePictureIntent != null) {
                    intentArray = new Intent[]{takePictureIntent};
                } else {
                    intentArray = new Intent[0];
                }
                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Bild auswählen");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE);
                return true;
            }
            // openFileChooser for Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                // Create AndroidExampleFolder at sdcard
                // Create AndroidExampleFolder at sdcard
                File imageStorageDir = new File(
                        Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES)
                        , "SportlerPlus");
                if (!imageStorageDir.exists()) {
                    // Create AndroidExampleFolder at sdcard
                    imageStorageDir.mkdirs();
                }
                // Create camera captured image file path and name
                File file = new File(
                        imageStorageDir + File.separator + "IMG_"
                                + String.valueOf(System.currentTimeMillis())
                                + ".jpg");
                mCapturedImageURI = Uri.fromFile(file);
                // Camera capture image intent
                final Intent captureIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                // Create file chooser intent
                Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
                // Set camera intent to file chooser
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                        , new Parcelable[] { captureIntent });
                // On select image call onActivityResult method of activity
                startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
            }
            // openFileChooser for Android < 3.0
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                openFileChooser(uploadMsg, "");
            }
            //openFileChooser for other Android versions
            public void openFileChooser(ValueCallback<Uri> uploadMsg,
                                        String acceptType,
                                        String capture) {
                openFileChooser(uploadMsg, acceptType);
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean userGesture, android.os.Message resultMsg)
            {
                WebView newWebView = new WebView(MainActivity.this);
                WebSettings popupWebSettings = newWebView.getSettings();
                popupWebSettings.setJavaScriptEnabled(true);
                popupWebSettings.setDomStorageEnabled(true);
                popupWebSettings.setUserAgentString(USER_AGENT);
                // Other configuration comes here, such as setting the WebViewClient
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(newWebView);

                newWebView.setWebChromeClient(new WebChromeClient() {
                    @Override
                    public void onCloseWindow(WebView window) {
                        dialog.dismiss();
                    }
                });
                newWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        // TODO: check usual URLs for Firebase JS Popup, open these in Dialog, others in external browser
                        if (url.contains("shop.sportlerplus.de")) {
                            MainActivity.this.openBrowser(url);
                            return false;
                        }
                        view.loadUrl(url);
                        dialog.show();
                        return true;
                    }
                });

                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

                ((WebView.WebViewTransport)resultMsg.obj).setWebView(newWebView);
                resultMsg.sendToTarget();
                return true;
            }
        });

        webView.addJavascriptInterface(new JavaScriptInterface(this), "AndroidJSInterface");

        Intent intent = getIntent();
        String action = intent.getAction();
        String url = baseUrl;
        if ("android.intent.action.VIEW".equalsIgnoreCase(action)) {
            //Uri data = intent.getData();
            String startUrl = intent.getDataString();
            if (!startUrl.contains("sportler-plus.web.app")) {
                openBrowser(startUrl);
            }
            else {
                url = startUrl;
                Log.d(TAG, "Startup with url=" + url);
                url = url.replace("/sharing/", "/");
            }
        }
        webView.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private void openCalendarApp(String calendarUrl) {
        try {
            String url = "https://calendar.google.com/calendar/render?cid=http://" +
                    calendarUrl.replaceFirst("webcal://", "");
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }
        catch(ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
    private void openBrowser(String url) {
        try {
            Uri uri = Uri.parse(url);
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getNoInternetURL() {
        return getString(R.string.no_internet_url);
    }

    /*public void handleInappPurchaseGenericError(final String debugMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Fehler beim Abschliessen des Kaufs.",
                        Toast.LENGTH_LONG).show();
            }
        });
    }*/

    /**
     * Is called by billing client if a user has successfully purchased something
     */
    /*public void handleInappPurchaseSuccessful(final PurchaseInterface purchase, final boolean reTransmit) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String id = purchase.getSku();
                String transaction = purchase.getPurchaseToken();
                String orderId = purchase.getOrderId();
                String successURL = String.format("%s?purchase=1&productId=%s&transactionId=%s&orderId=%s", baseUrl, id, transaction, orderId);
                webView.loadUrl(successURL);
            }
        });
    }

    public void handleInappPurchaseAlreadyBought(final String debugMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Du hast bereits diesen Kauf getätigt.",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void handleInappPurchaseCancelled() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Kauf abgebrochen.",
                        Toast.LENGTH_LONG).show();
            }
        });
    }
    */
    /**
     * is called if the user choosed a pending payment type, just as cash
     * @param purchase
     */
    /*
    public void handleInappPendingPurchase(final PurchaseInterface purchase) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String id = purchase.getSku();
                String transaction = purchase.getPurchaseToken();
                String orderId = purchase.getOrderId();
                //Normal behavior is calling the success function
                Toast.makeText(getApplicationContext(), "Dein Kauf befindet sich in Bearbeitung und kann zur Zeit noch nicht abgeschlossen werden.",
                        Toast.LENGTH_LONG).show();
            }
        });
    }
    */

    public class JavaScriptInterface implements GoogleFitCallbacks {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void shareImage(final String url) {
            verifyStoragePermissions(MainActivity.this);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(getApplicationContext()).load(url).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            try {
                                File bmpFile = createImageFile();
                                FileOutputStream outputStream = new FileOutputStream(bmpFile);

                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                                outputStream.flush();
                                outputStream.close();

                                Uri uri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeFile(bmpFile.getAbsolutePath()),null,null));
                                // use intent to share image
                                Intent share = new Intent(Intent.ACTION_SEND);
                                share.setType("image/*");
                                share.putExtra(Intent.EXTRA_STREAM, uri);
                                startActivity(Intent.createChooser(share, "Bild teilen"));
                            } catch(IOException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                        }
                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                        }
                    });
                }
            });

        }

        @JavascriptInterface
        public void shareTextUrl(final String title, final String text, final String url) {
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);

            final String html_body = String.format("<html><body><p><a href=\"%s\">%s</a></p><p>%s</p></body></html>",
                    url, title, text);
            final String text_body = title + "\n\n" + text + "\n\n" + url;
            // Add data to the intent, the receiving app will decide
            // what to do with it.
            share.putExtra(Intent.EXTRA_SUBJECT, title);
            share.putExtra(Intent.EXTRA_HTML_TEXT, html_body);
            share.putExtra(Intent.EXTRA_TEXT, text_body);

            startActivity(Intent.createChooser(share, "Teile den Link"));
        }

        @JavascriptInterface
        public void showToast(final String toast) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @JavascriptInterface
        public void copyToClipboard(String text) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("SportlerPlus Shop Code", text);
            clipboard.setPrimaryClip(clip);
        }

        @JavascriptInterface
        public void acquireWakeLock() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((Activity) mContext).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            });
        }

        @JavascriptInterface
        public void releaseWakeLock() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((Activity) mContext).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            });
        }

        @JavascriptInterface
        public void requestRotation(final String rotation) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if ("portrait".equalsIgnoreCase(rotation)) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                    }
                    else if ("landscape".equalsIgnoreCase(rotation)) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    }
                }
            });
        }

        @JavascriptInterface
        public void unlockRotation() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_USER);
                    } else {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                    }
                }
            });
        }

        @JavascriptInterface
        public void setBGColor(String color) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) { return; }

            if (color.equalsIgnoreCase("#ffffff")) {
                // Light status bar
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.white));// set status background white
                    }
                });
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Dark status bar
                        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.black));
                        View decorView = getWindow().getDecorView(); //set status background black
                        decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR); //set status text  light
                    }
                });
            }
        }

        @JavascriptInterface
        public void requestGoogleFitPermission() {
            verifyGoogleFitPermissions(MainActivity.this);
            GoogleFitHelper.getInstance().requestOAuthPermission();
        }

        @JavascriptInterface
        public void getTrackingData(String start, String end) {
            verifyGoogleFitPermissions(MainActivity.this);

            Date startDate = null, endDate = null;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            try {
                startDate = formatter.parse(start);
                endDate = formatter.parse(end);
            } catch (ParseException e) {
                //throw new RuntimeException(e);
                Log.d(TAG, "invalid day given");
                return;
            }

            GoogleFitHelper.getInstance().requestOAuthPermission();
            GoogleFitHelper.getInstance().getWorkoutsForDateRange(startDate, endDate, this);
        }

        @Override
        public void onWorkoutsReceived(List<GFWorkoutSession> sessionList) {
            GsonBuilder gson = new GsonBuilder();
            gson.registerTypeAdapter(GFWorkoutSession.class, new GFWorkoutSessionSerializer());
            Gson parser = gson.create();
            String workoutsJSON = parser.toJson(sessionList);
            Log.d(TAG, workoutsJSON);
            String jsCommand = String.format("if (window.vm && window.vm.trackedWorkoutsModalComponent) { let wd = %s; window.vm.trackedWorkoutsModalComponent.setTrackingData(wd); }",
                    workoutsJSON);
            ((MainActivity) mContext).evaluateJS(jsCommand);
        }

        @Override
        public void onWorkoutsError() {
            String jsCommand = String.format("if (window.vm && window.vm.trackedWorkoutModalComponent) { let wd = []; window.vm.trackedWorkoutModalComponent.setTrackingData(wd); }");
            ((MainActivity) mContext).evaluateJS(jsCommand);
        }
    }

}
